package com.android.semsx.smartshoosesdk;

import com.android.semsx.shooseeventsdk.SmartShooseMagager;
import com.android.semsx.shooseeventsdk.event.Event;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by young on 2016/5/4.
 */
public class SmartShooseService {

    public static void start() {
        Observable.interval(5, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        Map<String, Object> map = new HashMap<>();
                        map.put("Speed", 1.2);
                        SmartShooseMagager.INSTANCE.triggerEvent(new Event("TestEvent", map));
                    }
                });
    }

}
